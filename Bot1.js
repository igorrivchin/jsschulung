/* exported Bot1 */

function Bot1() {
  'use strict';

  // mögliche Richtungen für den nächsten Schritt
  var directions = ['up', 'right', 'down', 'left'];

  /**
   * Hier kannst Du Variablen anlegen, um Daten zwischen zwei Spielschritten
   * zu speichern. Diese Daten bleiben zwischen den Spielschritten erhalten.
   */
  var nextDirection = 1; // entspricht directions[nextDirection]; right

  /**
   * Diese Methode wird für jeden einzelnen Schritt des Bots aufgerufen. D.h. hier musst Du 
   * am Ende eine Richtung als String aus `directions` zurückgeben. In diese Richtung wird im
   * nächsten Zug gegangen. Dann wird diese Methode wieder aufgerufen, um die Richtung für den 
   * folgenden Zug zu bestimmen. Zusatz
   */
  this.onmessage = function (event) {
    /**
     * Füge hier Deinen Code ein, um 'nextDirection' zu ermitteln.
     */

    // Der String für die neue Richtung wird ermittelt.
    var direction = directions[nextDirection];

    // Rückgabe der Parameter 'id' und 'direction'. Dies brauchst Du nicht ändern. asdsad
    return { id: event.data.id, direction: direction };
  };

}
